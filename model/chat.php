<?php

/*
 * This file is part of FacturaScripts
 * Copyright (C) 2014-2015  Francesc Pineda Segarra  shawe.ewahs@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class chat extends fs_model
{

   public $id;
   public $user_id;
   public $message;
   public $sent_on;

   public function __construct( $chat = FALSE )
   {
      parent::__construct( 'chat', 'plugins/simple_ajax_chat/' );

      if ( $chat )
      {
         $this->id      = $chat[ 'id' ];
         $this->user_id = $chat[ 'user_id' ];
         $this->message = $chat[ 'message' ];
         $this->sent_on = $chat[ 'sent_on' ];
      }
      else
      {
         $this->id      = NULL;
         $this->user_id = NULL;
         $this->message = NULL;
         $this->sent_on = date('Y-m-d H:i:s');
      }
   }

   protected function install()
   {

   }

   public function save()
   {
      /* Sólo necesitamos guardar, y nunca actualizar */

      $sql = "INSERT INTO " . $this->table_name . " (user_id, message, sent_on ) "
               . "VALUES (". $this->var2str( $this->user_id )
               . ", " . $this->var2str( $this->message )
               . ", " . $this->var2str( $this->sent_on ). ");";

      if ( $this->db->exec( $sql ) )
      {
         $this->id = $this->db->lastval();
         return TRUE;
      }
      else
      {
         return FALSE;
      }
   }

   public function delete()
   {
      $sql = "DELETE FROM " . $this->table_name . " WHERE id = " . $this->var2str( $this->id ) . ";";

      return $this->db->exec( $sql );
   }

   public function getMessages()
   {
      $messages = array();

      $resultMessages = $this->db->select( "SELECT * FROM chat ORDER BY sent_on DESC;" );
      if( $resultMessages )
      {
         foreach ( $resultMessages as $row )
         {
            $messages[]=$row;
         }
      }

      return $messages;
   }

   public function addMessage($userId, $message)
   {
/*
      $this->chat->user_id =$userId;
      $this->chat->message = $message;

      $this->chat->save();
*/
      /* Sólo necesitamos guardar, y nunca actualizar */

      $sql = "INSERT INTO " . $this->table_name . " (user_id, message, sent_on ) "
               . "VALUES (". $this->var2str( $userId )
               . ", " . $this->var2str( $message )
               . ", " . $this->var2str( date('Y-m-d H:i:s') ). ");";

      if ( $this->db->exec( $sql ) )
      {
         $this->id = $this->db->lastval();
         return TRUE;
      }
      else
      {
         return FALSE;
      }
   }

   public function exists()
   {
      if ( is_null( $this->id ) )
      {
         return FALSE;
      }
      else
      {
         return $this->db->select( "SELECT * FROM " . $this->table_name .
                         " WHERE id = " . $this->var2str( $this->id ) . ";" );
      }
   }

}
