<?php

/*
 * This file is part of FacturaScripts
 * Copyright (C) 2014-2015  Francesc Pineda Segarra  shawe.ewahs@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_model( 'chat.php' );

class chats extends fs_controller
{

   public $messages;

   public function __construct()
   {
      parent::__construct( __CLASS__, 'Abrir chat', 'Chat' );
   }

   protected function private_core()
   {
      $this->share_extensions();

      $this->chat = new chat();

      if( isset( $_REQUEST['message'] ) )
      {
         if( !$this->chat->addMessage($this->user->nick, $_REQUEST['message']) )
         {
            foreach( $this->get_errors() as $err)
            {
               echo $err . "\n";
            }
         }
      }
      elseif( isset( $_GET['getNewMessages'] ) )
      {
         $this->template = FALSE;

         $this->messages = $this->chat->getMessages();
         $chat_conversation = array();

         if ( !empty($this->messages) )
         {
            $chat_conversation[] = '<table>';

            foreach ($this->messages as $message)
            {
               $msg = htmlentities($message['message'], ENT_NOQUOTES);
               $user_name = ucfirst($message['user_id']);
               $sent = $message['sent_on'];
               $chat_conversation[] = '
                  <tr class="msg-row-container">
                     <td>
                        <div class="msg-row">
                           <div class="avatar"></div>
                           <div class="message">
                              <span class="user-label">
                                 <a href="#" style="color: #6D84B4">'.$user_name.'</a>
                                 <span class="msg-time">'.$sent.'</span>
                              </span>
                              <br/>
                              '.$msg.'
                           </div>
                        </div>
                     </td>
                  </tr>';
            }
            $chat_conversation[] = '</table>';

         }
         else
         {
            $chat_conversation[] = '<span style="margin-left: 25px;">No hay mensajes disponibles.</span>';
         }

         echo implode ('',$chat_conversation);
      }
   }

   private function share_extensions()
   {
      foreach($this->extensions as $ext)
      {
         if( !file_exists($ext->text) )
         {
            $ext->delete();
         }
      }

      $extensions = array(
          array(
              'name' => 'jquery-ui',
              'page_from' => __CLASS__,
              'page_to' => __CLASS__,
              'type' => 'head',
              'text' => '<link href="plugins/simple_ajax_chat/view/css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet" type="text/css" />',
              'params' => ''
          ),
          array(
              'name' => 'core',
              'page_from' => __CLASS__,
              'page_to' => __CLASS__,
              'type' => 'head',
              'text' => '<link href="plugins/simple_ajax_chat/view/css/core.css" rel="stylesheet" type="text/css" />',
              'params' => ''
          ),


          array(
              'name' => 'chat.js',
              'page_from' => __CLASS__,
              'page_to' => __CLASS__,
              'type' => 'head',
              'text' => '<script type="text/javascript" src="plugins/simple_ajax_chat/view/js/chat.js"></script>',
              'params' => ''
          ),
      );
      foreach($extensions as $ext)
      {
         $fsext = new fs_extension($ext);
         $fsext->save();
      }
   }

}
